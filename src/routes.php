<?php

Route::get('calculator', function(){
	echo 'Hello from the calculator package!';
});
Route::get('add/{a}/{b}', 'Usthenet\GouvOfficers\CalculatorController@add');
Route::get('subtract/{a}/{b}', 'Usthenet\GouvOfficers\CalculatorController@subtract');