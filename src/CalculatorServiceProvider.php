<?php

namespace Usthenet\GouvOfficers;

use Illuminate\Support\ServiceProvider;

class CalculatorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Usthenet\GouvOfficers\CalculatorController');
        $this->loadViewsFrom(__DIR__.'/views', 'calculator');

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';

    }
}
